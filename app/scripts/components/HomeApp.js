//@flow
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { USER_ID } from '../constants'
import CommonComponent from './Common'
import { fetchUserBasic } from '../actions'


class RegularExpenses extends CommonComponent {
  render() {
    return (
      <ul className="demo-list-two mdl-list">
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>56,342</span>
            <span className="mdl-list__item-sub-title">Home Loan EMI</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <span className="mdl-list__item-secondary-info">Edit</span>
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>30,000</span>
            <span className="mdl-list__item-sub-title">SIP</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>1,700</span>
            <span className="mdl-list__item-sub-title">Health Insurance</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
      </ul>
    )
  }
}


class RegularIncome extends CommonComponent {
  render() {
    return (
      <ul className="demo-list-two mdl-list">
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>1,20,000</span>
            <span className="mdl-list__item-sub-title">Salary</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <span className="mdl-list__item-secondary-info">Edit</span>
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>25,0000</span>
            <span className="mdl-list__item-sub-title">Rent</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>15,000</span>
            <span className="mdl-list__item-sub-title">Royalties</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
      </ul>
    )
  }
}


class Goals extends CommonComponent {
  render() {
    return (
      <ul className="demo-list-two mdl-list">
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>55,000</span>
            <span className="mdl-list__item-sub-title">Xbox One</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <span className="mdl-list__item-secondary-info">Edit</span>
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
        <li className="mdl-list__item mdl-list__item--two-line">
          <span className="mdl-list__item-primary-content">
            <i className="material-icons mdl-list__item-avatar">person</i>
            <span>50,000</span>
            <span className="mdl-list__item-sub-title">Rolex</span>
          </span>
          <span className="mdl-list__item-secondary-content">
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">mode_edit</i>
            </button>
          </span>
        </li>
      </ul>
    )
  }
}
class HomeApp extends CommonComponent {
  constructor(props) {
    super(props)
    fetchUserBasic(USER_ID, this.props.dispatch)
  }

  render() {
    if (!this.props.basic) {
      return (
        <div className="mdl-grid">
          <div className="mdl-cell mdl-cell--6-offset">
            <div className="mdl-spinner mdl-js-spinner is-active"></div>;
          </div>
        </div>
      )
    }

    return (
      <div>
        <div className="mdl-grid">
          <div className="mdl-cell">
            <h4 className="highlight no-margin">Hello, Rahul</h4>
            <div id="account-details">
              <p className="no-margin">Account Num: {this.props.basic.accountno}</p>
              <p className="no-margin">Balance: {this.props.basic.balance}</p>
              <p className="no-margin">Net Worth: 1,00,00,000</p>
              <p className="no-margin">You Owe: 1,00,000</p>
            </div>
          </div>

          <div className="mdl-cell">
            <img src="images/gauge.png" alt="cibil score" />
            <p className="no-margin">Your cibil score is <span id="cibil-score">825</span></p>
            <p className="no-margin">
              <span id="cibil-expand">57.6% of new loans</span> with this score were approved last year.
            </p>
          </div>

          <div className="mdl-cell">
            <p className="no-margin highlight">Auto-approved loans</p>
            <table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
              <thead>
                <tr>
                  <th className="mdl-data-table__cell--non-numeric">Type</th>
                  <th className="mdl-data-table__cell--non-numeric">Amount</th>
                  <th className="mdl-data-table__cell--non-numeric">Apply</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">Personal Loan</td>
                  <td className="mdl-data-table__cell--non-numeric">10,00,000</td>
                  <td className="mdl-data-table__cell--non-numeric">
                    <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
                      <i className="material-icons">add</i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">Car Loan</td>
                  <td className="mdl-data-table__cell--non-numeric">12,00,000</td>
                  <td className="mdl-data-table__cell--non-numeric">
                    <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
                      <i className="material-icons">add</i>
                    </button>
                  </td>
                </tr>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">House Loan</td>
                  <td className="mdl-data-table__cell--non-numeric">1,10,00,000</td>
                  <td className="mdl-data-table__cell--non-numeric">
                    <button className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
                      <i className="material-icons">add</i>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="mdl-grid">
          <div className="mdl-cell" id="expenses">
            <h4>Regular Expenses</h4>
            <RegularExpenses />
          </div>

          <div className="mdl-cell" id="income">
            <h4>Regular Income</h4>
            <RegularIncome />
          </div>

          <div className="mdl-cell" id="goals">
            <h4>Goals</h4>
            <Goals />
          </div>
        </div>
      </div>
    )
  }

  static stateToProps(state, ownProps) {
    return {
      ...ownProps,
      ...state.userBasic
    }
  }
}


HomeApp = connect(HomeApp.stateToProps)(HomeApp)
export default HomeApp
