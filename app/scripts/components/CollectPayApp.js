//@flow
import React, { Component } from 'react'
import CommonComponent, { Switch, Checkbox, FloatingLabelTextBox, Select } from './Common'

class VirtualAddresses extends CommonComponent {
  render() {
    return (
      <div>
        <div className="mdl-grid">
          <div className="mdl-cell">
            <h4 className="highlight no-margin">Virtual Addresses</h4>
          </div>
        </div>
        <div className="mdl-grid">
          <div className="mdl-cell">
            <table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
              <thead>
                <tr>
                  <th className="mdl-data-table__cell--non-numeric">Id</th>
                  <th className="mdl-data-table__cell--non-numeric">Max Transaction Amount</th>
                  <th className="mdl-data-table__cell--non-numeric">Max Monthly Amount</th>
                  <th className="mdl-data-table__cell--non-numeric">Auto Approve</th>
                  <th className="mdl-data-table__cell--non-numeric"></th>
                  <th className="mdl-data-table__cell--non-numeric"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">rahul@uber.icici</td>
                  <td className="mdl-data-table__cell--non-numeric">1,000</td>
                  <td className="mdl-data-table__cell--non-numeric">5,000</td>
                  <td className="mdl-data-table__cell--non-numeric">YES</td>
                  <td className="mdl-data-table__cell--non-numeric" id="uber_edit">
                    <a className="pointer">
                      <i className="material-icons">mode_edit</i>
                    </a>
                    <div className="mdl-tooltip" htmlFor="uber_edit">
                      Edit this rule.
                    </div>
                  </td>
                  <td className="mdl-data-table__cell--non-numeric" id="uber_remove">
                    <a className="pointer">✖</a>
                    <div className="mdl-tooltip" htmlFor="uber_remove">
                      Remove this rule.
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">rahul@shopping.icici</td>
                  <td className="mdl-data-table__cell--non-numeric">5,000</td>
                  <td className="mdl-data-table__cell--non-numeric">15,000</td>
                  <td className="mdl-data-table__cell--non-numeric">NO</td>
                  <td className="mdl-data-table__cell--non-numeric" id="shopping_edit">
                    <a className="pointer">
                      <i className="material-icons">mode_edit</i>
                    </a>
                    <div className="mdl-tooltip" htmlFor="shopping_edit">
                      Remove this rule.
                    </div>
                  </td>
                  <td className="mdl-data-table__cell--non-numeric" id="shopping_remove">
                    <a className="pointer">✖</a>
                    <div className="mdl-tooltip" htmlFor="shopping_remove">
                      Remove this rule.
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">rahul@irctc.icici</td>
                  <td className="mdl-data-table__cell--non-numeric">5,000</td>
                  <td className="mdl-data-table__cell--non-numeric">15,000</td>
                  <td className="mdl-data-table__cell--non-numeric">YES</td>
                  <td className="mdl-data-table__cell--non-numeric" id="irctc_edit">
                    <a className="pointer">
                      <i className="material-icons">mode_edit</i>
                    </a>
                    <div className="mdl-tooltip" htmlFor="irctc_edit">
                      Remove this rule.
                    </div>
                  </td>
                  <td className="mdl-data-table__cell--non-numeric" id="irctc_remove">
                    <a className="pointer">✖</a>
                    <div className="mdl-tooltip" htmlFor="irctc_remove">
                      Remove this rule.
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}


class AddVirtualAddress extends CommonComponent {
  render() {
    return (
      <form>
        <div className="mdl-grid">
          <div className="mdl-cell mdl-cell--2-col">
            <FloatingLabelTextBox id="virtualAddressId"
              containerId="virual-address-id"
              label="Virtual Address Id" />
            <div className="mdl-tooltip" htmlFor="virtual-address-id">
              Virtual Address Unique Id
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col">
            <FloatingLabelTextBox id="virtualTransactionAmount" pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="virual-address-transaction-amount"
              label="Max Transaction Amount"
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="virtual-address-transaction-amount">
              Maximum allowed transaction amount.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col">
            <FloatingLabelTextBox id="virtualMonthlyLimit" pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="virual-address-monthly-limit"
              label="Monthly limit"
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="virtual-address-monthly-limit">
              Monthly limit.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{"margin-top": "2%"}}>
            <Select id="virtualAutoApprove">
              <option>Auto Approve</option>
              <option>Require MPIN</option>
            </Select>
          </div>

          <div className="mdl-cell" style={{"margin-top": "1.5%"}}>
            <button className="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect mdl-button--colored"
              type="submit">
              Add Virtual Address
            </button>
          </div>
        </div>
      </form>
    )
  }
}


class CollectRequests extends CommonComponent {
  render() {
    return (
      <div>
        <div className="mdl-grid">
          <div className="mdl-cell">
            <h4 className="highlight no-margin">Collect Requests</h4>
          </div>
        </div>
        <div className="mdl-grid">
          <div className="mdl-cell">
            <table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
              <thead>
                <tr>
                  <th className="mdl-data-table__cell--non-numeric">From</th>
                  <th className="mdl-data-table__cell--non-numeric">Amount</th>
                  <th className="mdl-data-table__cell--non-numeric">Remark</th>
                  <th className="mdl-data-table__cell--non-numeric"></th>
                  <th className="mdl-data-table__cell--non-numeric"></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">gold@gym</td>
                  <td className="mdl-data-table__cell--non-numeric">1,500</td>
                  <td className="mdl-data-table__cell--non-numeric">Monthly Membership</td>
                  <td className="mdl-data-table__cell--non-numeric" id="gold_approve">
                    <a className="pointer">✔</a>
                    <div className="mdl-tooltip" htmlFor="gold_approve">
                      Approve this request.
                    </div>
                  </td>
                  <td className="mdl-data-table__cell--non-numeric" id="gold_deny">
                    <a className="pointer">✖</a>
                    <div className="mdl-tooltip" htmlFor="gold_deny">
                      Rejest this request.
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">ujjawal@icici</td>
                  <td className="mdl-data-table__cell--non-numeric">768</td>
                  <td className="mdl-data-table__cell--non-numeric">Dinner Split</td>
                  <td className="mdl-data-table__cell--non-numeric" id="ujjawal_approve">
                    <a className="pointer">✔</a>
                    <div className="mdl-tooltip" htmlFor="ujjawal_approve">
                      Approve this request.
                    </div>
                  </td>
                  <td className="mdl-data-table__cell--non-numeric" id="ujjawal_deny">
                    <a className="pointer">✖</a>
                    <div className="mdl-tooltip" htmlFor="ujjawal_deny">
                      Rejest this request.
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="mdl-data-table__cell--non-numeric">accounting@icici</td>
                  <td className="mdl-data-table__cell--non-numeric">1,500</td>
                  <td className="mdl-data-table__cell--non-numeric">Company Outing</td>
                  <td className="mdl-data-table__cell--non-numeric" id="company_approve">
                    <a className="pointer">✔</a>
                    <div className="mdl-tooltip" htmlFor="company_approve">
                      Approve this request.
                    </div>
                  </td>
                  <td className="mdl-data-table__cell--non-numeric" id="company_deny">
                    <a className="pointer">✖</a>
                    <div className="mdl-tooltip" htmlFor="company_deny">
                      Rejest this request.
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}



class SendCollectRequest extends CommonComponent {
  render() {
    return (
      <form>
        <div className="mdl-grid">
          <div className="mdl-cell mdl-cell--2-col">
            <FloatingLabelTextBox
              containerId="collect-request-to"
              label="Send Request To" />
            <div className="mdl-tooltip" htmlFor="collect-request-to">
              Collect Request Recipient.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col">
            <FloatingLabelTextBox pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="collect-request-amount"
              label="Amount"
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="collect-request-amount">
              Amount to collect.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col">
            <FloatingLabelTextBox
              containerId="collect-request-remark"
              label="Remark" />
            <div className="mdl-tooltip" htmlFor="collect-request-remark">
              Remark
            </div>
          </div>

          <div className="mdl-cell" style={{"margin-top": "1.5%"}}>
            <button className="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect mdl-button--colored"
              type="submit">
              Send Collect Request
            </button>
          </div>
        </div>
      </form>
    )
  }
}


class CollectPayApp extends CommonComponent {
  render() {
    return (
      <div>
        <VirtualAddresses />
        <AddVirtualAddress />
        <CollectRequests />
        <SendCollectRequest />
      </div>
    )
  }
}

export default CollectPayApp
