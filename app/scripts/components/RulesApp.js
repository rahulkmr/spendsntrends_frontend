//@flow
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { USER_ID } from '../constants'
import { fetchCurrentRules, removeRules } from '../actions'
import CreateEditRule from './CreateEditRule'
import CommonComponent from './Common'

class Rule extends CommonComponent {
  render() {
    if (!this.props.rule) {
      return <tr></tr>
    }
    let timeStart, timeEnd, spendLimitDecline, spendLimitAlert, spendLimitType
    let uniqueId = (this.props.rule.controlType || "GLOBAL")
    let ruleType = (this.props.rule.controlType || "GLOBAL").replace('TCT_', '').replace('_', ' ')
    if (this.props.rule.timeRange) {
      timeStart = this.props.rule.timeRange.startTime
      timeEnd = this.props.rule.timeRange.endTime
    }
    if (this.props.rule.spendLimit) {
      spendLimitDecline = `${this.props.rule.spendLimit.declineThreshold}(${this.props.rule.spendLimit.type})`
      spendLimitAlert = `${this.props.rule.spendLimit.alertThreshold}(${this.props.rule.spendLimit.type})`
    }
    return (
      <tr>
        <td className="mdl-data-table__cell--non-numeric">{ruleType}</td>
        <td className="mdl-data-table__cell--non-numeric">{this.props.rule.shouldDeclineAll && '✔'}</td>
        <td className="mdl-data-table__cell--non-numeric">{this.props.rule.shouldAlertOnDecline && '✔'}</td>
        <td>{this.props.rule.declineThreshold}</td>
        <td>{this.props.rule.alertThreshold}</td>
        <td className="mdl-data-table__cell--non-numeric">{timeStart}</td>
        <td className="mdl-data-table__cell--non-numeric">{timeEnd}</td>
        <td className="mdl-data-table__cell--non-numeric">{spendLimitDecline}</td>
        <td className="mdl-data-table__cell--non-numeric">{spendLimitAlert}</td>
        <td className="mdl-data-table__cell--non-numeric" id={uniqueId}>
          <a className="pointer" onClick={(evt) => this._remove(evt)}>✖</a>
          <div className="mdl-tooltip" htmlFor={uniqueId}>
            Remove this rule.
          </div>
        </td>
      </tr>
    )
  }

  _remove(evt) {
    evt.preventDefault()
    let data = {
      isControlEnabled: true,
      shouldDeclineAll: false
    }
    if (this.props.rule.controlType) {
      data = {
        transactionControls: [{
          ...data,
          controlType: this.props.rule.controlType
        }]
      }
    } else {
      data = {
        globalControl: {
          ...data
        }
      }
    }
    removeRules(USER_ID, data, this.props.dispatch)
  }
}


class CurrentRules extends CommonComponent {
  constructor({userId, globalControl, transactionControls, dispatch}) {
    super({userId, globalControl, transactionControls, dispatch})
  }

  render() {
    return (
      <table className="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
        <thead>
          <tr>
            <th className="mdl-data-table__cell--non-numeric">Type</th>
            <th className="mdl-data-table__cell--non-numeric">Decline All</th>
            <th className="mdl-data-table__cell--non-numeric">Alert On Decline</th>
            <th>Decline ></th>
            <th>Alert ></th>
            <th className="mdl-data-table__cell--non-numeric">Date Start</th>
            <th className="mdl-data-table__cell--non-numeric">Date End</th>
            <th className="mdl-data-table__cell--non-numeric">Spend Decline ></th>
            <th className="mdl-data-table__cell--non-numeric">Spend Alert ></th>
            <th className="mdl-data-table__cell--non-numeric"></th>
          </tr>
        </thead>
        <tbody>
          <Rule rule={this.props.globalControl} dispatch={this.props.dispatch} />
          {this.props.transactionControls.map((rule) =>
                                              <Rule key={rule.controlType} rule={rule}
                                              dispatch={this.props.dispatch} />)}
        </tbody>
      </table>
    )
  }

}


class RulesApp extends CommonComponent {
  constructor({documentID, globalControl, transactionControls, removingRules, dispatch}) {
    super({documentID, globalControl, transactionControls, removingRules, dispatch})
    fetchCurrentRules(USER_ID, dispatch)
  }

  render() {
    if (!this.props.documentID) {
      return this._spinner()
    } else {
      return (
        <div>
          <div className="mdl-grid">
          <div style={{display: this.props.removingRules || 'none'}}>
            <div id="p2" className="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
          </div>
            <h4 className="highlight">Current Rules</h4>
            <CurrentRules userId={USER_ID} globalControl={this.props.globalControl}
              transactionControls={this.props.transactionControls}
              dispatch={this.props.dispatch} />
          </div>
          <div className="mdl-grid">
            <h4 className="highlight">Add/Change Rule</h4>
          </div>
          <CreateEditRule userId={USER_ID} documentID={this.props.documentID} />
        </div>
      )
    }
  }

  _spinner() {
    return (
      <div className="mdl-grid">
        <div className="mdl-cell mdl-cell--6-offset">
          <div className="mdl-spinner mdl-js-spinner is-active"></div>;
        </div>
      </div>
    )
  }

  static stateToProps(state) {
    return {
      documentID: state.currentRules.documentID,
      globalControl: state.currentRules.globalControl,
      transactionControls: state.currentRules.transactionControls,
      removingRules: state.currentRules.removingRules
    }
  }
}

RulesApp = connect(RulesApp.stateToProps)(RulesApp)

export default RulesApp
