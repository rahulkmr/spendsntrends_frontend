//@flow
import React, { Component } from 'react'
import CommonComponent from './Common'

class AboutApp extends CommonComponent {
  render() {
    return (
      <div className="mdl-grid">
        <div className="mdl-cell mdl-cell--4-offset">
          <h2 className="highlight">SpendsNTrends</h2>
          <p className="secondary-highlight">Track your spending and optimizie it.</p>
        </div>
      </div>
    )
  }
}

export default AboutApp
