//@flow
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pikaday from 'react-pikaday'
import CommonComponent, { Switch, Checkbox, FloatingLabelTextBox, Select } from './Common'
import * as actions from '../actions'
import { SPEND_TYPE_WEEKLY, SPEND_TYPE_MONTHLY, SPEND_TYPE_DAILY } from '../constants'


class CreateEditRule extends CommonComponent {
  constructor({userId, documentID, shouldDeclineAll, shouldAlertOnDecline, declineThreshold, alertThreshold,
  timeRange, timeRangeFrom, timeRangeTo, spendLimit, spendLimitAlertThreshol, spendLimitDeclineThreshol, dispatch}) {
    super({userId, documentID, shouldDeclineAll, shouldAlertOnDecline, declineThreshold, alertThreshold,
          timeRange, timeRangeFrom, timeRangeTo, spendLimit, spendLimitAlertThreshol, spendLimitDeclineThreshol, dispatch})
  }

  render() {
    return (
      <form id="create-edit-rule" onSubmit={(e) => this._handleSubmit(e)}>
        <div className="mdl-grid">
          <div className="mdl-cell mdl-cell--2-col">
            <Switch containerId="rule-should-decline-all" id="shouldDeclineAll" label="Decline All"
              value={this.props.shouldDeclineAll}
              onChange={(evt) => this.props.dispatch(actions.toggleDeclineAll())}
              checked={this.props.shouldDeclineAll} />
            <div className="mdl-tooltip" htmlFor="rule-should-decline-all">
              Deny all transactions.
              Do this if you have lost your card or to prevent misuse.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col">
            <Checkbox id="shouldAlertOnDecline" label="Alert On Decline"
              containerId="rule-alert-on-decline"
              value={this.props.shouldAlertOnDecline}
              onChage={(evt) => this.props.dispatch(actions.toggleAlertOnDecline())}
              checked={this.props.shouldAlertOnDecline} />
            <div className="mdl-tooltip" htmlFor="rule-alert-on-decline">
              Alert when a transaction is declined.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.shouldDeclineAll && 'none'}}>
            <FloatingLabelTextBox id="alertThreshold" pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="rule-alert-threshold"
              value={this.props.alertThreshold} label="Alert Threshold"
              onChange={(evt) => this.props.dispatch(actions.setAlertThreshold(evt.target.value))}
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="rule-alert-threshold">
              Send alert when transaction exceeds this amount.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.shouldDeclineAll && 'none'}}>
            <FloatingLabelTextBox id="declineThreshold" pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="rule-decline-threshold"
              value={this.props.declineThreshold} label="Decline Threshold"
              onChange={(evt) => this.props.dispatch(actions.setDeclineThreshold(evt.target.value))}
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="rule-decline-threshold">
              Decline transactions exceeding this amount.
            </div>
          </div>

        </div>

        <div className="mdl-grid" style={{display: this.props.shouldDeclineAll && 'none'}}>
          <div className="mdl-cell mdl-cell--2-col">
            <Switch id="timeRange" label="For Date Range"
              containerId="rule-time-range"
              onChange={(evt) => this.props.dispatch(actions.toggleTimeRange())}
              value={!!this.props.timeRange}
              checked={!!this.props.timeRange} />
            <div className="mdl-tooltip" htmlFor="rule-time-range">
              You can define a time range for which the above limits should apply.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.timeRange || 'none'}}>
            <div id="rule-time-range-from">
              <p className="muted">From:</p>
              <Pikaday value={this.props.timeRangeFrom}
                onChange={(date) => this.props.dispatch(actions.setTimeRangeFrom(date))}/>
            </div>
            <div className="mdl-tooltip" htmlFor="rule-time-range-from">
              From date. You can set it to today or in the future.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.timeRange || 'none'}}>
            <div id="rule-time-range-to">
              <p className="muted">To:</p>
              <Pikaday value={this.props.timeRangeTo}
                onChange={(date) => this.props.dispatch(actions.setTimeRangeTo(date))} />
            </div>
            <div className="mdl-tooltip" htmlFor="rule-time-range-to">
              To date. You can set it to today or in the future.
            </div>
          </div>
        </div>


        <div className="mdl-grid" style={{display: this.props.shouldDeclineAll && 'none'}}>
          <div className="mdl-cell mdl-cell--2-col">
            <Switch id="spendLimit" label="Set Spend Limit"
              containerId="rule-spend-limit"
              onChange={(evt) => this.props.dispatch(actions.toggleSpendLimit())}
              value={!!this.props.spendLimit}
              checked={!!this.props.spendLimit} />
            <div className="mdl-tooltip" htmlFor="rule-spend-limit">
              Set spend limit for a given time duration(day, week, month)
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.spendLimit || 'none'}}>
            <Select id="spendType" containerId="rule-spend-type"
              onChange={(evt) => this.props.dispatch(actions.setSpendLimitType(evt.target.value))}>
              <option value={SPEND_TYPE_MONTHLY}>Monthly</option>
              <option value={SPEND_TYPE_WEEKLY}>Weekly</option>
              <option value={SPEND_TYPE_DAILY}>Daily</option>
            </Select>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.spendLimit || 'none'}}>
            <FloatingLabelTextBox id="spendLimitAlertThreshold" pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="rule-spend-type-alert-threshold"
              value={this.props.spendTypeAlertThreshold} label="Alert Threshold"
              onChange={(evt) => this.props.dispatch(actions.setSpendLimitAlertThreshold(evt.target.value))}
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="rule-spend-type-alert-threshold">
              Send alert when transaction exceeds this amount.
            </div>
          </div>

          <div className="mdl-cell mdl-cell--2-col" style={{display: this.props.spendLimit || 'none'}}>
            <FloatingLabelTextBox id="spendLimitDeclineThreshold" pattern="-?[0-9]*(\.[0-9]+)?"
              containerId="rule-spend-type-decline-threshold"
              value={this.props.spendTypeDeclineThreshold} label="Decline Threshold"
              onChange={(evt) => this.props.dispatch(actions.setSpendLimitDeclineThreshold(evt.target.value))}
              errorMessage="Input is not a number!" />
            <div className="mdl-tooltip" htmlFor="rule-spend-type-decline-threshold">
              Decline transactions exceeding this amount.
            </div>
          </div>
        </div>

        <div className="mdl-grid" style={{display: this.props.savingRules || 'none'}}>
          <div id="p2" className="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>

        <div className="mdl-grid">
          <div className="mdl-cell mdl-cell--1-col">
            <button className="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect mdl-button--colored"
              type="submit">
              Save
            </button>
          </div>
          <div className="mdl-cell mdl-cell--1-col">
            <button className="mdl-button mdl-button--raised mdl-js-button mdl-js-ripple-effect mdl-button--accent"
              type="reset">
              Reset
            </button>
          </div>
        </div>
      </form>
    )
  }

  _handleSubmit(evt) {
    evt.preventDefault()
    let data = {
      isControlEnabled: true,
      shouldDeclineAll: !!this.props.shouldDeclineAll,
      shouldAlertOnDecline: !!this.props.shouldAlertOnDecline,
      declineThreshold: this.props.declineThreshold,
      alertThreshold: this.props.alertThreshold
    }
    if (this.props.timeRange) {
      data.timeRange = {
        startTime: this.props.timeRangeFrom,
        endTime: this.props.timeRangeTo
      }
    }
    if (this.props.spendLimit) {
      data.spendLimit = {
        type: this.props.spendLimitType,
        currentPeriodSpend: 0,
        currentPeriodEnd: Date(),
        declineThreshold: this.props.spendLimitDeclineThreshold,
        alertThreshold: this.props.spendLimitAlertThreshold,
        timeZoneID: 'GMT'
      }
    }
    actions.addRules(this.props.userId, {globalControl: data}, this.props.dispatch)
  }

  static stateToProps(state, ownProps) {
    return {
      ...ownProps,
      ...state.createEditRule
    }
  }


}

CreateEditRule = connect(CreateEditRule.stateToProps)(CreateEditRule)
export default CreateEditRule
