//@flow
import React, { Component } from 'react'


export default class CommonComponent extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    window.componentHandler.upgradeDom()
  }

  componentDidUpdate() {
    window.componentHandler.upgradeDom()
  }

}

export class Switch extends CommonComponent {
  render() {
    return (
      <div className={this.props.containerClass} id={this.props.containerId}>
        <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor={this.props.id}>
          <input type="checkbox" id={this.props.id} className="mdl-switch__input"
            value={this.props.value}
            disabled={this.props.disabled}
            onChange={this.props.onChange}
            ref={this.props.ref}
            checked={this.props.checked} />
          <span className="mdl-switch__label">{this.props.label}</span>
        </label>
      </div>
    )
  }
}


export class Checkbox extends CommonComponent {
  render() {
    return (
      <div className={this.props.containerClass} id={this.props.containerId}>
        <label className="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" htmlFor={this.props.id}>
          <input type="checkbox" id={this.props.id} className="mdl-checkbox__input"
            value={this.props.value}
            disabled={this.props.disabled}
            onChange={this.props.onChange}
            ref={this.props.ref}
            defaultChecked={this.props.checked} />
          <span className="mdl-checkbox__label">{this.props.label}</span>
        </label>
      </div>
    )
  }
}


export class FloatingLabelTextBox extends CommonComponent {
  render() {
    return (
      <div className={this.props.containerClass} id={this.props.containerId}>
        <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
          <input className="mdl-textfield__input" type="text" pattern={this.props.pattern}
            disabled={this.props.disabled}
            onChange={this.props.onChange}
            ref={this.props.ref}
            id={this.props.id} value={this.props.value} />
          <label className="mdl-textfield__label" htmlFor={this.props.id}>{this.props.label}</label>
          <span className="mdl-textfield__error">{this.props.errorMessage}</span>
        </div>
      </div>
    )
  }
}


export class Select extends CommonComponent {
  render() {
    return (
      <div className={this.props.containerClass} id={this.props.containerId}>
        <select name={this.props.Id} id={this.props.id} value={this.props.value}
        disabled={this.props.disabled}
        onChange={this.props.onChange}
        ref={this.props.ref}>
          {this.props.children}
        </select>
      </div>
    )
  }
}
