//@flow
import React, { Component } from 'react'
import { Link } from 'react-router'
import CommonComponent from './Common'

class Navigation extends CommonComponent {
  render() {
    return (
      <nav className={this.props.classes}>
        <Link className="mdl-navigation__link" to="/" onlyActiveOnIndex={true} activeClassName="current-link">
          <i className="material-icons">home</i>Home
        </Link>
        <Link className="mdl-navigation__link" to="/rules" activeClassName="current-link">
          <i className="material-icons">pan_tool</i>Transaction Control
        </Link>
        <Link className="mdl-navigation__link" to="/payments" activeClassName="current-link">
          <i className="material-icons">assessment</i>Collect & Pay
        </Link>
        <Link className="mdl-navigation__link" to="/about" activeClassName="current-link">
          <i className="material-icons">info</i>About
        </Link>
      </nav>
    )
  }
}


class Footer extends CommonComponent {
  render() {
    return (
      <footer className="mdl-mega-footer">
        <div className="mdl-mega-footer__middle-section">

          <div className="mdl-mega-footer__drop-down-section">
            <input className="mdl-mega-footer__heading-checkbox" type="checkbox" defaultChecked={true} />
            <h1 className="mdl-mega-footer__heading">Features</h1>
            <ul className="mdl-mega-footer__link-list">
              <li><a href="#">About</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Partners</a></li>
            </ul>
          </div>

          <div className="mdl-mega-footer__drop-down-section">
            <input className="mdl-mega-footer__heading-checkbox" type="checkbox" defaultChecked={true} />
            <h1 className="mdl-mega-footer__heading">Technology</h1>
            <ul className="mdl-mega-footer__link-list">
              <li><a href="#">How it works</a></li>
              <li><a href="#">Patterns</a></li>
              <li><a href="#">Usage</a></li>
            </ul>
          </div>

          <div className="mdl-mega-footer__drop-down-section">
            <input className="mdl-mega-footer__heading-checkbox" type="checkbox" defaultChecked={true} />
            <h1 className="mdl-mega-footer__heading">FAQ</h1>
            <ul className="mdl-mega-footer__link-list">
              <li><a href="#">Q & A</a></li>
              <li><a href="#">Contact us</a></li>
            </ul>
          </div>
        </div>

        <div className="mdl-mega-footer__bottom-section">
          <div className="mdl-logo">SpendsNTrends</div>
          <ul className="mdl-mega-footer__link-list">
            <li><a href="#">Help</a></li>
            <li><a href="#">Privacy & Terms</a></li>
          </ul>
        </div>

      </footer>

    )
  }
}


class App extends CommonComponent {
  render() {
    return (
      <div>
        <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
            <header className="mdl-layout__header">
            <div className="mdl-layout__header-row">
                <Link id="title" to="/"><span className="mdl-layout-title">SpendsNTrends</span></Link>
                <Navigation classes="mdl-navigation mdl-layout--large-screen-only"/>
            </div>
            </header>

            <div className="mdl-layout__drawer">
                <span className="mdl-layout-title">SpendsNtrends</span>
                <Navigation classes="mdl-navigation" />
            </div>

            <main className="mdl-layout__content">
            <div className="page-content">
              {this.props.children}
            </div>
            <div id="snackbar" className="mdl-js-snackbar mdl-snackbar">
              <div className="mdl-snackbar__text"></div>
              <button className="mdl-snackbar__action" type="button"></button>
            </div>
            </main>

        </div>
      </div>
    )
  }
}

export default App
