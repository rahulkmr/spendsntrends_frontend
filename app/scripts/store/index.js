//@flow
import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import reducers from '../reducers'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'

const logger = createLogger()
const store = createStore(reducers,
                          compose(applyMiddleware(thunkMiddleware, logger),
                          window.devToolsExtension ? window.devToolsExtension() : f => f)
                         )

export default store
