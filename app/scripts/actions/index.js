//@flow
import request from 'superagent'
import { current_rules_url, add_rules_url, remove_rules_url, user_basic_url } from '../common'
import * as constants from '../constants'

export const INITIALIZE_RULES = 'INITIALIZE_RULES'
export const TOGGLE_DECLINE_ALL = 'TOGGLE_DECLINE_ALL'
export const TOGGLE_TIME_RANGE = 'TOGGLE_TIME_RANGE'
export const TOGGLE_SPEND_LIMIT = 'TOGGLE_SPEND_LIMIT'
export const TOGGLE_ALERT_ON_DECLINE = 'TOGGLE_ALERT_ON_DECLINE'
export const SET_ALERT_THRESHOLD = 'SET_ALERT_THRESHOLD'
export const SET_DECLINE_THRESHOLD = 'SET_DECLINE_THRESHOLD'
export const SET_TIME_RANGE_FROM = 'SET_TIME_RANGE_FROM'
export const SET_TIME_RANGE_TO = 'SET_TIME_RANGE_TO'
export const SET_SPEND_LIMIT_ALERT_THRESHOLD = 'SET_SPEND_LIMIT_ALERT_THRESHOLD'
export const SET_SPEND_LIMIT_DECLINE_THRESHOLD = 'SET_SPEND_LIMIT_DECLINE_THRESHOLD'
export const SET_SPEND_LIMIT_TYPE = 'SET_SPEND_LIMIT_TYPE'
export const SAVING_RULES = 'SAVING_RULES'
export const REMOVING_RULES = 'REMOVING_RULES'
export const RULES_SAVED = 'RULES_SAVED'
export const RULES_REMOVED = 'RULES_REMOVED'
export const INITIALIZE_USER_BASIC = 'INITIALIZE_USER_BASIC'


export const savingRules = () => {
  return {
    type: SAVING_RULES
  }
}


export const removingRules = () => {
  return {
    type: REMOVING_RULES
  }
}


export const fetchCurrentRules = (user_id, dispatch) => {
  request.get(current_rules_url(user_id))
  .end((err, res) => dispatch(initializeRules(res.body.data)))
}


export const fetchUserBasic = (user_id, dispatch) => {
  request.get(user_basic_url(user_id))
  .end((err, res) => dispatch(initializeUserBasic(res.body.data)))
}


export const addRules = (user_id, rules, dispatch) => {
  dispatch(savingRules())
  request.post(add_rules_url(user_id))
  .send(rules)
  .end((err, res) => {
    dispatch(rulesSaved())
    dispatch(initializeRules(res.body.data))
  })
}


export const removeRules = (user_id, rules, dispatch) => {
  dispatch(removingRules())
  request.delete(remove_rules_url(user_id))
  .send(rules)
  .end((err, res) => {
    dispatch(rulesRemoved())
    dispatch(initializeRules(res.body.data))
  })
}


export const initializeRules = (data) => {
  return {
    type: INITIALIZE_RULES,
    data: data
  }
}


export const initializeUserBasic = (data) => {
  return {
    type: INITIALIZE_USER_BASIC,
    data: data
  }
}


export const rulesSaved = () => {
  const snackbar = window.document.querySelector(`#${constants.SNACKBAR_ID}`)
  snackbar.MaterialSnackbar.showSnackbar({message: 'Rule Saved', timeout: 2000})
  return {
    type: RULES_SAVED
  }
}


export const rulesRemoved = () => {
  const snackbar = window.document.querySelector(`#${constants.SNACKBAR_ID}`)
  snackbar.MaterialSnackbar.showSnackbar({message: 'Rule Removed', timeout: 2000})
  return {
    type: RULES_REMOVED
  }
}


export const toggleDeclineAll = () => {
  return {
    type: TOGGLE_DECLINE_ALL
  }
}


export const toggleTimeRange = () => {
  return {
    type: TOGGLE_TIME_RANGE
  }
}


export const toggleSpendLimit = () => {
  return {
    type: TOGGLE_SPEND_LIMIT
  }
}


export const toggleAlertOnDecline = () => {
  return {
    type: TOGGLE_ALERT_ON_DECLINE
  }
}


export const setAlertThreshold = (amt) => {
  return {
    type: SET_ALERT_THRESHOLD,
    data: amt
  }
}


export const setDeclineThreshold = (amt) => {
  return {
    type: SET_DECLINE_THRESHOLD,
    data: amt
  }
}


export const setTimeRangeFrom = (from) => {
  return {
    type: SET_TIME_RANGE_FROM,
    data: from
  }
}


export const setTimeRangeTo = (to) => {
  return {
    type: SET_TIME_RANGE_TO,
    data: to
  }
}


export const setSpendLimitAlertThreshold = (amt) => {
  return {
    type: SET_SPEND_LIMIT_ALERT_THRESHOLD,
    data: amt
  }
}


export const setSpendLimitDeclineThreshold = (amt) => {
  return {
    type: SET_SPEND_LIMIT_DECLINE_THRESHOLD,
    data: amt
  }
}


export const setSpendLimitType = (type) => {
  return {
    type: SET_SPEND_LIMIT_TYPE,
    data: type
  }
}
