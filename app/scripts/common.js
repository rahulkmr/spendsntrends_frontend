//@flow
import { API_BASE } from './constants'

export const current_rules_url = (user_id) => `${API_BASE}/users/${user_id}/current_rules`
export const add_rules_url = (user_id) =>  `${API_BASE}/users/${user_id}/add_rules`
export const remove_rules_url = (user_id) =>  `${API_BASE}/users/${user_id}/remove_rules`
export const user_basic_url = (user_id) => `${API_BASE}/users/${user_id}`
