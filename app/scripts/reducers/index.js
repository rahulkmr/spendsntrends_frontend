//@flow
import { combineReducers } from 'redux'
import * as actions from '../actions'
import { SPEND_TYPE_MONTHLY } from '../constants'


const userBasic = (state = {}, action) => {
  switch (action.type) {
      case actions.INITIALIZE_USER_BASIC:
          return action.data
      default:
          return state
  }
}


const currentRules = (state = {}, action) => {
  switch (action.type) {
      case actions.INITIALIZE_RULES:
          return action.data
      case actions.REMOVING_RULES:
          return {...state, removingRules: true}
      case actions.RULES_REMOVED:
          return {...state, removingRules: false}
      default:
          return state
  }
}


const createEditRuleState = {shouldAlertOnDecline: true, spendLimitType: SPEND_TYPE_MONTHLY}

const createEditRule = (state = createEditRuleState, action) => {
  switch (action.type) {
      case actions.TOGGLE_DECLINE_ALL:
          return {...state, shouldDeclineAll: !state.shouldDeclineAll}
      case actions.TOGGLE_TIME_RANGE:
          return {...state, timeRange: !state.timeRange}
      case actions.TOGGLE_SPEND_LIMIT:
          return {...state, spendLimit: !state.spendLimit}
      case actions.TOGGLE_ALERT_ON_DECLINE:
          return {...state, shouldAlertOnDecline: !state.shouldAlertOnDecline}
      case actions.SET_ALERT_THRESHOLD:
          return {...state, alertThreshold: action.data}
      case actions.SET_DECLINE_THRESHOLD:
          return {...state, declineThreshold: action.data}
      case actions.SET_TIME_RANGE_FROM:
          return {...state, timeRangeFrom: action.data}
      case actions.SET_TIME_RANGE_TO:
          return {...state, timeRangeTo: action.data}
      case actions.SET_SPEND_LIMIT_TYPE:
          return {...state, spendLimitType: action.data}
      case actions.SET_SPEND_LIMIT_ALERT_THRESHOLD:
          return {...state, spendLimitAlertThreshold: action.data}
      case actions.SET_SPEND_LIMIT_DECLINE_THRESHOLD:
          return {...state, spendLimitDeclineThreshold: action.data}
      case actions.SAVING_RULES:
          return {...state, savingRules: true}
      case actions.RULES_SAVED:
          return {...createEditRuleState, savingRules: false}
      default:
          return state
  }
}


const reducers = combineReducers({
  userBasic,
  currentRules,
  createEditRule
})

export default reducers
