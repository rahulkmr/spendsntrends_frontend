//@flow
export const API_BASE = 'http://localhost:5000'
export const USER_ID = 1
export const SPEND_TYPE_DAILY = 'LMT_DAY'
export const SPEND_TYPE_WEEKLY = 'LMT_WEEK'
export const SPEND_TYPE_MONTHLY = 'LMT_MONTH'
export const SNACKBAR_ID = 'snackbar'
