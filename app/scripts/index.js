//@flow
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './components/App'
import HomeApp from './components/HomeApp'
import RulesApp from './components/RulesApp'
import AboutApp from './components/AboutApp'
import CollectPayApp from './components/CollectPayApp'
import store from './store'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={HomeApp} />
        <Route path="rules" component={RulesApp} />
        <Route path="about" component={AboutApp} />
        <Route path="payments" component={CollectPayApp} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
